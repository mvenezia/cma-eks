package cmd

import (
	"flag"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
	"strings"
)

var (
	RootCmd = &cobra.Command{
		Use:              "cma-eks",
		Short:            "The CMA EKS Helper",
		Long:             `The CMA EKS Helper`,
		TraverseChildren: true,
	}
)

func init() {
	viper.SetEnvPrefix("cmaeks")
	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)

	// using standard library "flag" package
	RootCmd.PersistentFlags().String("kubeconfig", "", "Location of kubeconfig file")
	RootCmd.PersistentFlags().String("kubectl", "kubectl", "Location of kubectl file")
	RootCmd.PersistentFlags().String("kubernetes-namespace", "default", "What namespace to operate on")
	RootCmd.PersistentFlags().String("aws-region", "us-east-1", "The AWS region")
	RootCmd.PersistentFlags().String("aws-access-key-id", "", "The AWS Access Key ID")
	RootCmd.PersistentFlags().String("aws-secret-access-key", "", "The AWS Secret Access Key")

	viper.BindPFlag("kubeconfig", RootCmd.PersistentFlags().Lookup("kubeconfig"))
	viper.BindPFlag("kubectl", RootCmd.PersistentFlags().Lookup("kubectl"))
	viper.BindPFlag("kubernetes-namespace", RootCmd.PersistentFlags().Lookup("kubernetes-namespace"))
	viper.BindPFlag("aws-region", RootCmd.PersistentFlags().Lookup("aws-region"))
	viper.BindPFlag("aws-access-key-id", RootCmd.PersistentFlags().Lookup("aws-access-key-id"))
	viper.BindPFlag("aws-secret-access-key", RootCmd.PersistentFlags().Lookup("aws-secret-access-key"))

	viper.AutomaticEnv()
	RootCmd.Flags().AddGoFlagSet(flag.CommandLine)
}

func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
