package vpccmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/cma-eks/pkg/util/awsutil"
)

func init() {
	VPCCmd.AddCommand(generateCreateVPCCmd())
}

func generateCreateVPCCmd() *cobra.Command {
	var createVPCCmd = &cobra.Command{
		Use:   "create [vpcName]",
		Short: "Creates a VPC",
		Long:  `Creates an AWS VPC`,
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			createVPC(args)
		},
	}

	return createVPCCmd
}

func createVPC(args []string) {
	result, err := awsutil.CreateEKSControlPlaneForStack(args[0])
//	result, err := awsutil.GenerateEKSRole(args[0])
//	result, err := awsutil.DeployEKSVPCCFStack(awsmodels.EKSVPCOptions{Name: args[0]})
	if err != nil {
		fmt.Printf("Error creating VPC: %s\n", err)
	}
	fmt.Printf("VPC Created in CloudFormation Stack: %s\n", result)
}
