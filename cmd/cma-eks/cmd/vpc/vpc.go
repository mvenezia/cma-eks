package vpccmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/cma-eks/cmd/cma-eks/cmd"
)

var VPCCmd = generateVPCCmd()

func init() {
	cmd.RootCmd.AddCommand(VPCCmd)
}

func generateVPCCmd() *cobra.Command {
	var createVPCCmd = &cobra.Command{
		Use:              "vpc",
		Short:            "AWS VPC commands",
		Long:             `AWS VPC Commands`,
		TraverseChildren: true,
	}

	return createVPCCmd
}
