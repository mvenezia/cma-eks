package main

import (
	"gitlab.com/mvenezia/cma-eks/cmd/cma-eks/cmd"
	_ "gitlab.com/mvenezia/cma-eks/cmd/cma-eks/cmd/vpc"
)

func main() {
	cmd.Execute()
}
